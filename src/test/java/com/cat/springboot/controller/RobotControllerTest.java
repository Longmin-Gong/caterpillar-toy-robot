package com.cat.springboot.controller;

import com.cat.springboot.RobotApplication;
import com.cat.springboot.entity.RobotCommand;
import com.cat.springboot.entity.RobotFacing;
import com.cat.springboot.entity.RobotPosition;
import com.cat.springboot.manager.RobotManager;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = RobotApplication.class)
@AutoConfigureMockMvc
public class RobotControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    @MockBean
    RobotManager robotManager;

    @Test
    public void testPlace() throws Exception {
        RobotPosition position = new RobotPosition() {{
            setX(0);
            setY(0);
            setFacing(RobotFacing.NORTH);
        }};
        Mockito.when(robotManager.place(any())).thenReturn(position);
        mockMvc.perform(MockMvcRequestBuilders
                .post("/place")
                .content(this.mapper.writeValueAsString(position))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.x", equalTo(0)))
                .andExpect(jsonPath("$.y", equalTo(0)))
                .andExpect(jsonPath("$.facing", equalTo("NORTH")));
    }

    @Test
    public void testMove() throws Exception {
        RobotPosition position = new RobotPosition() {{
            setX(0);
            setY(1);
            setFacing(RobotFacing.NORTH);
        }};
        Mockito.when(robotManager.doCommand(RobotCommand.MOVE)).thenReturn(position);
        mockMvc.perform(MockMvcRequestBuilders
                .post("/MOVE")
                .content(this.mapper.writeValueAsString(position))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.x", equalTo(0)))
                .andExpect(jsonPath("$.y", equalTo(1)))
                .andExpect(jsonPath("$.facing", equalTo("NORTH")));
    }

    @Test
    public void testLeft() throws Exception {
        RobotPosition position = new RobotPosition() {{
            setX(0);
            setY(1);
            setFacing(RobotFacing.WEST);
        }};
        Mockito.when(robotManager.doCommand(RobotCommand.LEFT)).thenReturn(position);
        mockMvc.perform(MockMvcRequestBuilders
                .post("/LEFT")
                .content(this.mapper.writeValueAsString(position))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.x", equalTo(0)))
                .andExpect(jsonPath("$.y", equalTo(1)))
                .andExpect(jsonPath("$.facing", equalTo("WEST")));
    }

    @Test
    public void testRight() throws Exception {
        RobotPosition position = new RobotPosition() {{
            setX(0);
            setY(1);
            setFacing(RobotFacing.SOUTH);
        }};
        Mockito.when(robotManager.doCommand(RobotCommand.RIGHT)).thenReturn(position);
        mockMvc.perform(MockMvcRequestBuilders
                .post("/RIGHT")
                .content(this.mapper.writeValueAsString(position))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.x", equalTo(0)))
                .andExpect(jsonPath("$.y", equalTo(1)))
                .andExpect(jsonPath("$.facing", equalTo("SOUTH")));
    }

    @Test
    public void testReport() throws Exception {
        RobotPosition position = new RobotPosition() {{
            setX(0);
            setY(1);
            setFacing(RobotFacing.SOUTH);
        }};
        Mockito.when(robotManager.doCommand(RobotCommand.REPORT)).thenReturn(position);
        mockMvc.perform(MockMvcRequestBuilders
                .post("/REPORT")
                .content(this.mapper.writeValueAsString(position))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.x", equalTo(0)))
                .andExpect(jsonPath("$.y", equalTo(1)))
                .andExpect(jsonPath("$.facing", equalTo("SOUTH")));
    }
}
