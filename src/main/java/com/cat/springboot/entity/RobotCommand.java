package com.cat.springboot.entity;

/**
 * @author longmingong
 */
public enum RobotCommand {
    MOVE, LEFT, RIGHT, REPORT
}
