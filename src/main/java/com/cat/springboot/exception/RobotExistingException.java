package com.cat.springboot.exception;

/**
 * @author longmingong
 */
public class RobotExistingException extends RuntimeException{
    public RobotExistingException(String message) {
        super(message);
    }
}
