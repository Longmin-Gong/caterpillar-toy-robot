package com.cat.springboot.manager;

import com.cat.springboot.entity.RobotCommand;
import com.cat.springboot.entity.RobotFacing;
import com.cat.springboot.entity.RobotPosition;
import com.cat.springboot.exception.RobotBadRequestException;
import com.cat.springboot.exception.RobotExistingException;
import com.cat.springboot.exception.RobotIndexOutOfBoundsException;
import com.cat.springboot.exception.RobotNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author longmingong
 */
@Service
public class RobotManager {

    private static final int INDEX_SIZE = 4;

    private RobotPosition position;

    /**
     * Validation {@link RobotPosition} when placing the robot on the table.
     *
     * @param robotPosition requested postion for placing robot
     * @throws RuntimeException
     */
    private void validate(RobotPosition robotPosition) throws RobotBadRequestException, RobotIndexOutOfBoundsException {
        if (robotPosition.getX() == null || robotPosition.getY() == null || robotPosition.getFacing() == null) {
            throw new RobotBadRequestException("X|Y|Facing is not nullable.");
        }
        if (robotPosition.getX() > INDEX_SIZE || robotPosition.getX() < 0 || robotPosition.getY() > INDEX_SIZE || robotPosition.getY() < 0) {
            throw new RobotIndexOutOfBoundsException("X|Y should between 0 to 4");
        }
    }

    /**
     * Throw an exception when player want to place the robot on the table twice.
     *
     * @param robotPosition {@link RobotPosition} initial robot position.
     * @return placed robot position
     * @throws RobotExistingException
     */
    public RobotPosition place(RobotPosition robotPosition) throws RobotExistingException {
        validate(robotPosition);
        if (position == null) {
            position = robotPosition;
        } else {
            throw new RobotExistingException("Toy robot already exists.");
        }
        return position;
    }

    /**
     * Execute all commands
     *
     * @param command robot command
     * @return Robot position after executing command
     */
    public RobotPosition doCommand(RobotCommand command) {
        if (this.position == null) {
            throw new RobotNotFoundException("ROBOT MISSING");
        }
        switch (command) {
            case MOVE:
                move();
                break;
            case LEFT:
            case RIGHT:
                RobotFacing facing = position.getFacing().turn(command);
                position.setFacing(facing);
                break;
            case REPORT:
            default:
                break;
        }
        RobotPosition result = position;
        // stop this game once report is executed.
        if (command == RobotCommand.REPORT) {
            position = null;
        }
        return result;
    }

    /**
     * Movement logic.
     */
    private void move() {
        if (position.getFacing() == RobotFacing.NORTH) {
            if (position.getY() != INDEX_SIZE) {
                position.setY(position.getY() + 1);
            }
        } else if (position.getFacing() == RobotFacing.SOUTH) {
            if (position.getY() != 0) {
                position.setY(position.getY() - 1);
            }
        } else if (position.getFacing() == RobotFacing.WEST) {
            if (position.getX() != 0) {
                position.setX(position.getX() - 1);
            }
        } else {
            if (position.getX() != INDEX_SIZE) {
                position.setX(position.getX() + 1);
            }
        }
    }
}
