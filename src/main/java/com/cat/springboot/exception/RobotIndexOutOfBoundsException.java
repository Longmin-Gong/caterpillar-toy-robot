package com.cat.springboot.exception;

/**
 * @author longmingong
 */
public class RobotIndexOutOfBoundsException extends RuntimeException{
    public RobotIndexOutOfBoundsException(String message) {
        super(message);
    }
}
