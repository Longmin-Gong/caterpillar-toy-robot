package com.cat.springboot.exception;

/**
 * @author longmingong
 */
public class RobotBadRequestException extends RuntimeException{
    public RobotBadRequestException(String message) {
        super(message);
    }
}
