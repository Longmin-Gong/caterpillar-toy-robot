package com.cat.springboot.controller;

import com.cat.springboot.entity.RobotCommand;
import com.cat.springboot.entity.RobotPosition;
import com.cat.springboot.exception.RobotBadRequestException;
import com.cat.springboot.exception.RobotExistingException;
import com.cat.springboot.exception.RobotIndexOutOfBoundsException;
import com.cat.springboot.exception.RobotNotFoundException;
import com.cat.springboot.manager.RobotManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author longmingong
 */
@RestController("/robot")
public class RobotController {

    private RobotManager robotManager;

    @Autowired
    public RobotController(RobotManager robotManager) {
        this.robotManager = robotManager;
    }

    @PostMapping("/place")
    public ResponseEntity<RobotPosition> place(@RequestBody RobotPosition robotPosition) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(this.robotManager.place(robotPosition));
    }

    @PostMapping("/{command}")
    public ResponseEntity doCommand(@PathVariable("command") RobotCommand command) {
        return ResponseEntity.ok(this.robotManager.doCommand(command));
    }

    @ExceptionHandler({RobotNotFoundException.class,
            RobotExistingException.class,
            RobotBadRequestException.class,
            RobotIndexOutOfBoundsException.class})
    public ResponseEntity handleException(RuntimeException exception) {
        return ResponseEntity.badRequest().body(exception.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity handleException(Exception exception) {
        exception.printStackTrace();
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body("Oops,exception throws in server.");
    }
}
