package com.cat.springboot.entity;

/**
 * @author longmingong
 */
public enum RobotFacing {
    NORTH, SOUTH, EAST, WEST;

    public RobotFacing turn(RobotCommand command) {
        RobotFacing result = null;
        switch (command) {
            case LEFT:
                if (this.equals(NORTH)) {
                    result = WEST;
                } else if (this.equals(WEST)) {
                    result = SOUTH;
                } else if (this.equals(SOUTH)) {
                    result = EAST;
                } else {
                    result = NORTH;
                }
                break;
            case RIGHT:
                if (this.equals(NORTH)) {
                    result = EAST;
                } else if (this.equals(EAST)) {
                    result = SOUTH;
                } else if (this.equals(SOUTH)) {
                    result = WEST;
                } else {
                    result = NORTH;
                }
                break;
            default:
                result = this;
        }
        return result;
    }
}
