package com.cat.springboot.manager;

import com.cat.springboot.entity.RobotCommand;
import com.cat.springboot.entity.RobotFacing;
import com.cat.springboot.entity.RobotPosition;
import com.cat.springboot.exception.RobotBadRequestException;
import com.cat.springboot.exception.RobotExistingException;
import com.cat.springboot.exception.RobotIndexOutOfBoundsException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test class for {@link RobotManager}
 *
 * @author longmingong
 */
public class RobotManagerTest {

    RobotManager robotManager = new RobotManager();

    @Test
    public void testValidationWithNullX() {
        RobotPosition invalidPosition = new RobotPosition() {{
            setX(null);
        }};
        RuntimeException runtimeException = assertThrows(RobotBadRequestException.class, () -> robotManager.place(invalidPosition));
        String expectedMessage = "X|Y|Facing is not nullable.";
        String actualMessage = runtimeException.getMessage();
        assertTrue(actualMessage.equals(expectedMessage));
    }

    @Test
    public void testValidationWithIndexOutOfBoundsX() {
        RobotPosition invalidPosition = new RobotPosition() {{
            setX(5);
            setY(0);
            setFacing(RobotFacing.NORTH);
        }};
        RuntimeException runtimeException = assertThrows(RobotIndexOutOfBoundsException.class, () -> robotManager.place(invalidPosition));
        String expectedMessage = "X|Y should between 0 to 4";
        String actualMessage = runtimeException.getMessage();
        assertTrue(actualMessage.equals(expectedMessage));
    }

    @Test
    public void testPlaceWithValidPosition() {
        RobotPosition validPosition = new RobotPosition() {{
            setX(0);
            setY(0);
            setFacing(RobotFacing.NORTH);
        }};
        RobotPosition result = robotManager.place(validPosition);
        assertTrue(result.getX() == validPosition.getX());
        assertTrue(result.getY() == validPosition.getY());
        assertTrue(result.getFacing() == validPosition.getFacing());
    }

    @Test
    public void testPlaceWithAlreadyExistException() {
        RobotPosition validPosition = new RobotPosition() {{
            setX(0);
            setY(0);
            setFacing(RobotFacing.NORTH);
        }};
        RobotPosition result = robotManager.place(validPosition);
        RuntimeException exception = assertThrows(RobotExistingException.class, () -> robotManager.place(validPosition));
        String expectedMessage = "Toy robot already exists.";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.equals(expectedMessage));
    }

    @Test
    public void testDoCommandWithNormalCase() {
        RobotPosition validPosition = new RobotPosition() {{
            setX(0);
            setY(0);
            setFacing(RobotFacing.NORTH);
        }};
        robotManager.place(validPosition);
        // test move
        RobotPosition position = robotManager.doCommand(RobotCommand.MOVE);
        assertTrue(position.getY() == 1);

        // test ignore out of bounds
        robotManager.doCommand(RobotCommand.MOVE);
        robotManager.doCommand(RobotCommand.MOVE);
        robotManager.doCommand(RobotCommand.MOVE);
        robotManager.doCommand(RobotCommand.MOVE);
        position = robotManager.doCommand(RobotCommand.MOVE);
        assertTrue(position.getY() == 4);

        // test turn left
        position = robotManager.doCommand(RobotCommand.LEFT);
        assertTrue(position.getFacing() == RobotFacing.WEST);

        // test ignore out of bounds
        position = robotManager.doCommand(RobotCommand.MOVE);
        assertTrue(position.getX() == 0);

        // test turn right
        robotManager.doCommand(RobotCommand.RIGHT);
        robotManager.doCommand(RobotCommand.RIGHT);
        position = robotManager.doCommand(RobotCommand.MOVE);
        assertTrue(position.getX() == 1);
        assertTrue(position.getFacing() == RobotFacing.EAST);

        // test report command
        position = robotManager.doCommand(RobotCommand.REPORT);
        assertTrue(position.getX() == 1);
        assertTrue(position.getFacing() == RobotFacing.EAST);
        // position is cleared, so player can re-place it again
        RobotPosition rePlacePosition = new RobotPosition() {{
            setX(0);
            setY(0);
            setFacing(RobotFacing.NORTH);
        }};
        position = robotManager.place(rePlacePosition);
        assertTrue(position.getX() == 0);
        assertTrue(position.getY() == 0);
        assertTrue(position.getFacing() == RobotFacing.NORTH);
    }
}
