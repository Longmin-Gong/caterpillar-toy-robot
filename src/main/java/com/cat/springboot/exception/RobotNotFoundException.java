package com.cat.springboot.exception;

/**
 * @author longmingong
 */
public class RobotNotFoundException extends RuntimeException{
    public RobotNotFoundException(String message) {
        super(message);
    }
}
