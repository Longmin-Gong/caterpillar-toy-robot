# caterpillar-toy-robot
Toy robot for caterpillar to handle robot movement.

### OpenAPI docs
#### API doc: http://localhost:8080/swagger-ui.html

#### Implemented endpoints
- [x] place a robot
- [x] command left
- [x] command right
- [x] command move
- [x] command report, after report command, this game will be ended by default.

### Technologies
- [x] Docker
- [x] Spring Boot
- [x] Swagger OpenAPI

### How to build project
`gradle build --no-daemon`

### How to build docker image
`docker image build . -t {repository}/{image-name}:{image-version}`

### How to set up dev environment
- Prerequisite
    - [x] docker, docker compose
    - [x] gradle
- start RobotApplication in your IDE

### How to start this docker image
- `docker-compose up -d`
- `visit` http://localhost:8080/swagger-ui.html

