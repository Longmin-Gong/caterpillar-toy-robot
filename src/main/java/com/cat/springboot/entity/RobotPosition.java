package com.cat.springboot.entity;

/**
 * @author longmingong
 */
public class RobotPosition {
    private Integer x;
    private Integer y;
    private RobotFacing facing;

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public RobotFacing getFacing() {
        return facing;
    }

    public void setFacing(RobotFacing facing) {
        this.facing = facing;
    }

    @Override
    public String toString() {
        return x + "," + y + "," + facing;
    }
}
