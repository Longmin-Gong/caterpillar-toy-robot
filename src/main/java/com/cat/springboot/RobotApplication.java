package com.cat.springboot;

import com.cat.springboot.entity.RobotFacing;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author longmingong
 */
@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Toy robot application", version = "1.0"))
public class RobotApplication {
    public static void main(String[] args) {
        SpringApplication.run(RobotApplication.class, args);
    }
}
